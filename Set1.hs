{-# LANGUAGE OverloadedStrings #-}
module Set1 where

import           Data.Bits
import           Data.Maybe
import           Data.Either
import           Data.List              as L (elemIndex, elemIndices)
import           Data.Word8
import           Prelude                as P
import           Data.Map               as M
import           Data.ByteString        as B
import qualified Data.ByteString.Base64 as B64
import qualified Data.ByteString.Base16 as B16
import           Crypto.Cipher
import qualified Data.ByteString.Char8  as C
import           Data.Set               as S


-- Challenge 1

decode16 :: ByteString -> ByteString
decode16 input = fineOrError $ B16.decode input
    where fineOrError (x, "") = x
          fineOrError (_, x)  = error . show $ B.append "decode16: couldn't decode character " (B.take 1 x)

decode64 :: ByteString -> ByteString
decode64 input = either (error . show) id (B64.decode input)

challenge1 :: ByteString -> ByteString
challenge1 input = B64.encode $ decode16 input

challenge1Bonus :: ByteString -> ByteString
challenge1Bonus input = decode16 input


-- Challenge 2

fixedXOR :: ByteString -> ByteString -> ByteString
fixedXOR str1 str2 = pack $ B.zipWith xor str1 str2

challenge2 :: ByteString -> ByteString -> ByteString
challenge2 str1 str2 = B16.encode $ fixedXOR (decode16 str1) (decode16 str2)

challenge2Bonus :: ByteString -> ByteString -> ByteString
challenge2Bonus str1 str2 = pack $ B.zipWith xor (decode16 str1) (decode16 str2)


-- Challenge 3

type CharCount = Map Word8 Int
type CharFreqs = Map Word8 Float

addChar :: CharCount -> Word8 -> CharCount
addChar freq c = insertWith (+) c 1 freq

normalize :: Integral a => a -> a -> Float
normalize 0 _ = error "normalize: can't normalize with length of 0"
normalize len x = fromIntegral x / fromIntegral len

makeCharFreqs :: ByteString -> CharFreqs
makeCharFreqs str = M.map (normalize $ B.length str) charCount
  where charCount = B.foldl addChar M.empty str

getCharScore :: CharFreqs -> Word8 -> Float
getCharScore freq char = findWithDefault 0 char freq

score ::  CharFreqs -> ByteString -> Float
score freq str = sum $ P.map (getCharScore freq) (unpack str)

allSingleXORs :: ByteString -> [ByteString]
allSingleXORs str = fmap (fixedXOR str) chars
    where chars = [B.replicate (B.length str) x | x <- [0..255]]

best :: CharFreqs -> [ByteString] -> ByteString
best freq list = P.foldl bestScore "" list
    where bestScore a b = if (score freq a) > (score freq b) then a else b

breakSingleXOR :: CharFreqs -> ByteString -> ByteString
breakSingleXOR freq input = best freq $ allSingleXORs input

singleXORKey :: CharFreqs -> ByteString -> Word8
singleXORKey freq input = fromIntegral . fromJust $ L.elemIndex (breakSingleXOR freq input) (allSingleXORs input)

challenge3 :: CharFreqs -> ByteString -> ByteString
challenge3 freq input = breakSingleXOR freq (decode16 input)

challenge3Bonus :: CharFreqs -> ByteString -> Word8
challenge3Bonus freq input = singleXORKey freq (decode16 input)


-- Challenge 4

bruteforceSingleXORList :: CharFreqs -> [ByteString] -> ByteString
bruteforceSingleXORList freq list = best freq bestPerLine
    where bestPerLine = P.map (breakSingleXOR freq) list

challenge4 :: CharFreqs -> ByteString -> ByteString
challenge4 freq input = bruteforceSingleXORList freq list
    where list = P.map decode16 $ B.split _lf $ input


-- Challenge 5

splitWithLength :: Int -> ByteString -> [ByteString]
splitWithLength _ "" = []
splitWithLength l input = B.take l input : splitWithLength l (B.drop l input)

repeatingXOR :: ByteString -> ByteString -> ByteString
repeatingXOR key input = B.concat $ P.map (fixedXOR key) (splitWithLength (B.length key) input)

challenge5 :: ByteString -> ByteString -> ByteString
challenge5 ct key = B16.encode $ repeatingXOR key ct


-- Challenge 6

hamming :: ByteString -> ByteString -> Int
hamming str1 str2 = sum $ P.map popCount (B.zipWith xor str1 str2)

type Distances = Map Int Float

addDistance :: (ByteString, ByteString) -> Distances -> Distances
addDistance (str1, str2) dist
  | B.length str1 /= B.length str2 = error "addDistance: lenghts of inputs are not equal"
  | otherwise = M.insert (B.length str1) (normalize (B.length str1) (hamming str1 str2)) dist

-- the 4 here was tweaked manually. Other values work too, but 1 was pathological
slices :: ByteString -> Int -> (ByteString, ByteString)
slices input len = (B.take len input, B.take len $ B.drop (len*4) input)

makeDistances :: ByteString -> Distances
makeDistances ct = P.foldr addDistance M.empty $ P.map (slices ct) [2..40]

likelyKeysize :: ByteString -> Int
likelyKeysize ct = P.head [k | (k, v) <- M.toList dist, v == (P.minimum $ M.elems dist)]
    where dist = makeDistances ct

findVigenereKey :: CharFreqs -> ByteString -> ByteString
findVigenereKey freq ct = B.pack $ P.map (singleXORKey freq) blocks
    where blocks = B.transpose $ splitWithLength (likelyKeysize ct) ct

-- most inputs are base64 encoded and we want to remove the newlines as well
clean :: ByteString -> ByteString
clean = decode64 . B.filter (/= _lf)

challenge6Key :: CharFreqs -> ByteString -> ByteString
challenge6Key freq ct = findVigenereKey freq $ clean ct

challenge6 :: CharFreqs -> ByteString -> ByteString
challenge6 freq ct = repeatingXOR (findVigenereKey freq $ clean ct) $ clean ct


-- Challenge 7
-- using libghc-cryptocipher-dev

initAES128 :: ByteString -> AES128
initAES128 = either (error . show) cipherInit . makeKey

challenge7 :: ByteString -> ByteString -> ByteString
challenge7 key = ecbDecrypt cipher . clean
    where cipher = initAES128 key


-- Challenge 8

prepareFile :: ByteString -> [ByteString]
prepareFile = P.map decode16 . C.words

hasDuplicate :: Ord a => [a] -> Bool
hasDuplicate l = dup l S.empty
    where
        dup [] _ = False
        dup (x:xs) h
          | S.member x h = True
          | otherwise  = dup xs (S.insert x h)

detectECB :: Int -> ByteString -> Bool
detectECB ks ct = hasDuplicate $ splitWithLength ks ct

challenge8 :: ByteString -> [Int]
challenge8 input = L.elemIndices True $ P.map (detectECB 16) (prepareFile input)
