{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Exception
import           Data.ByteString        (ByteString)
import           Set1
import           Set2
import           System.Random
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8  as C

testChallenge9 :: Bool
testChallenge9 = expected == pkcs7 20 input
    where expected = "YELLOW SUBMARINE\x04\x04\x04\x04"
          input = "YELLOW SUBMARINE"

testChallenge10 :: ByteString -> ByteString
testChallenge10 = decryptCBC iv key . clean
    where iv = B.replicate 16 0
          key = "YELLOW SUBMARINE"

testChallenge11 :: RandomGen g => [g] -> [ByteString] -> [Bool]
testChallenge11 gs pts = zipWith challenge11 gs pts

testChallenge12 :: RandomGen g => g -> Bool
testChallenge12 g = isECB && expected == baatDecryption (baatOracle g)
    where isECB = oracleECB enc (oracleBlockSize enc)
          enc = baatOracle g
          expected = "Rollin' in my 5.0\n\
                     \With my rag-top down so my hair can blow\n\
                     \The girlies on standby waving just to say hi\n\
                     \Did you stop? No, I just drove by\n\SOH"

testChallenge13 :: RandomGen g => g -> Bool
testChallenge13 g = isAdmin $ decryptProfile g (ecbCutAndPaste (cutAndPasteOracle g))

testChallenge14 :: RandomGen g => g -> Bool
testChallenge14 g = expected == baatPrefixDecryption (baatPrefixOracle g)
    where expected = "Rollin' in my 5.0\n\
                     \With my rag-top down so my hair can blow\n\
                     \The girlies on standby waving just to say hi\n\
                     \Did you stop? No, I just drove by\n\SOH"

testChallenge15 :: Bool
testChallenge15 = and [one, not two, not three]
    where one  = challenge15 "ICE ICE BABY\x04\x04\x04\x04"
          two  = challenge15 "ICE ICE BABY\x05\x05\x05\x05"
          three = challenge15 "ICE ICE BABY\x01\x02\x03\x04"

testChallenge16 :: RandomGen g => g -> Bool
testChallenge16 g = detectAdmin g (cbcBitflipAttack (cbcBitflipOracle g))

main :: IO ()
main = do
    putStrLn "Challenge 9"
    putStrLn "-----------"
    putStrLn $ assert testChallenge9 "Challenge 9 passed"
    putStrLn ""

    putStrLn "Challenge 10"
    putStrLn "-----------"
    file10 <- B.readFile "data/10.txt"
    C.putStr $ testChallenge10 file10
    putStrLn ""

    putStrLn "Challenge 11"
    putStrLn "-----------"
    pt1 <- B.readFile "data/dracula"
    g1 <- newStdGen
    g2 <- newStdGen
    putStrLn $ assert (and $ testChallenge11 [g1, g2] [pt1, pt1]) "Challenge 11 passed"
    putStrLn ""

    putStrLn "Challenge 12"
    putStrLn "-----------"
    g <- newStdGen
    C.putStrLn $ baatDecryption (baatOracle g)
    putStrLn $ assert (testChallenge12 g) "Challenge 12 passed"
    putStrLn ""

    putStrLn "Challenge 13"
    putStrLn "-----------"
    putStrLn $ assert (testChallenge13 g) "Challenge 13 passed"
    putStrLn ""

    putStrLn "Challenge 14"
    putStrLn "-----------"
    putStrLn $ assert (testChallenge14 g) "Challenge 14 passed"
    putStrLn ""

    putStrLn "Challenge 15"
    putStrLn "-----------"
    putStrLn $ assert testChallenge15 "Challenge 15 passed"
    putStrLn ""

    putStrLn "Challenge 16"
    putStrLn "-----------"
    putStrLn $ assert (testChallenge16 g) "Challenge 16 passed"
    putStrLn ""
