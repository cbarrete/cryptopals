{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Exception
import           Data.ByteString        (ByteString)
import           Set1
import qualified Data.ByteString        as B
import qualified Data.ByteString.Char8  as C

testChallenge1 :: Bool
testChallenge1 = challenge1 input == expected
    where input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
          expected = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"

testChallenge1Bonus :: ByteString
testChallenge1Bonus = challenge1Bonus input
    where input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"

testChallenge2 :: Bool
testChallenge2 = challenge2 str1 str2 == expected
    where str1 = "1c0111001f010100061a024b53535009181c"
          str2 = "686974207468652062756c6c277320657965"
          expected = "746865206b696420646f6e277420706c6179"

testChallenge2Bonus :: ByteString
testChallenge2Bonus = challenge2Bonus str1 str2
    where str1 = "1c0111001f010100061a024b53535009181c"
          str2 = "686974207468652062756c6c277320657965"

testChallenge3 :: CharFreqs -> Bool
testChallenge3 freq = challenge3 freq input == "Cooking MC's like a pound of bacon"
    where input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

testChallenge3Bonus :: CharFreqs -> ByteString
testChallenge3Bonus freq = B.append "the key was: " $ B.pack [challenge3Bonus freq input]
    where input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

testChallenge4 :: CharFreqs -> ByteString -> Bool
testChallenge4 ref input = challenge4 ref input == "Now that the party is jumping\n"

testChallenge5 :: Bool
testChallenge5 = challenge5 input "ICE" == expected
    where input = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
          expected = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"

testChallenge7 :: ByteString -> ByteString
testChallenge7 = challenge7 key
    where key = "YELLOW SUBMARINE"

main :: IO ()
main = do
    putStrLn "Challenge 1"
    putStrLn "-----------"
    C.putStrLn testChallenge1Bonus
    putStrLn $ assert testChallenge1 "Challenge 1 passed"
    putStrLn ""

    putStrLn "Challenge 2"
    putStrLn "-----------"
    C.putStrLn testChallenge2Bonus
    putStrLn $ assert testChallenge2 "Challenge 2 passed"
    putStrLn ""

    putStrLn "Challenge 3"
    putStrLn "-----------"
    dracula <- B.readFile "data/dracula"
    let freq = makeCharFreqs dracula
    C.putStrLn $ testChallenge3Bonus freq
    putStrLn $ assert (testChallenge3 freq) "Challenge 3 passed"
    putStrLn ""

    putStrLn "Challenge 4"
    putStrLn "-----------"
    file4 <- B.readFile "data/4.txt"
    putStrLn $ assert (testChallenge4 freq file4) "Challenge 4 passed"
    putStrLn ""

    putStrLn "Challenge 5"
    putStrLn "-----------"
    putStrLn $ assert testChallenge5 "Challenge 5 passed"
    putStrLn ""

    putStrLn "Challenge 6"
    putStrLn "-----------"
    file6 <- B.readFile "data/6.txt"
    C.putStr $ challenge6Key freq file6
    C.putStr $ challenge6 freq file6
    putStrLn ""

    putStrLn "Challenge 7"
    putStrLn "-----------"
    file7 <- B.readFile "data/7.txt"
    C.putStr $ testChallenge7 file7
    putStrLn ""

    putStrLn "Challenge 8"
    putStrLn "-----------"
    file8 <- B.readFile "data/8.txt"
    putStrLn "ECB detected at lines:"
    print $ challenge8 file8
    putStrLn ""
