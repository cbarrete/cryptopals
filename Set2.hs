{-# LANGUAGE OverloadedStrings #-}
module Set2 where

import           Control.Monad
import           Crypto.Cipher
import           Data.ByteString       as B
import           Data.Maybe
import qualified Data.List             as L
import           Data.Word8
import           Network.URL
import           Prelude               as P
import           Set1
import           System.Random         as R
import qualified Data.ByteString.Char8 as C


-- Challenge 9

pkcs7 :: Int -> ByteString -> ByteString
pkcs7 bs input = B.append input (B.replicate padLen (fromIntegral padLen))
    where padLen
            | mod (B.length input) bs == 0 = 0
            | otherwise = bs - (mod (B.length input) bs)

-- undo pkcs7, Nothing if not a valid pkcs7-padded string
unPkcs7 :: ByteString -> Maybe ByteString
unPkcs7 input = go input (B.last input) (B.last input)
    where go :: (Num a, Eq a) => ByteString -> Word8 -> a -> Maybe ByteString
          go str _ 0 = Just str
          go str byte i
            | (B.last str) /= byte = Nothing
            | otherwise            = go (B.init str) byte (i-1)


-- Challenge 10

-- encrypts a block of plaintext with the last block of ciphertext (can be the iv)
-- returns the new (extended) ciphertext
encryptBlockCBC :: AES128 -> ByteString -> ByteString -> ByteString
encryptBlockCBC cipher ct = B.append ct . (ecbEncrypt cipher . fixedXOR lastCtBlock)
    where lastCtBlock = B.drop (B.length ct - 16) ct

-- pretend that the iv is part of the ciphertext and strip it when done
encryptCBC :: ByteString -> ByteString -> ByteString -> ByteString
encryptCBC iv key pt = B.drop 16 $ P.foldl (encryptBlockCBC cipher) iv blocks
    where blocks = splitWithLength 16 pt
          cipher = initAES128 key

-- decrypt one block at a time with map and xor everything at the end
decryptCBC :: ByteString -> ByteString -> ByteString -> ByteString
decryptCBC iv key ct = repeatingXOR xorString (B.concat $ P.map (ecbDecrypt cipher) blocks)
    where xorString = B.append iv (B.take (B.length ct - 16) ct)
          cipher = initAES128 key
          blocks = splitWithLength 16 ct


-- Challenge 11

randByteString :: RandomGen g => g -> Int -> ByteString
randByteString g n = pack . P.take n $ randoms g

randomAESKey :: RandomGen g => g -> ByteString
randomAESKey g = randByteString g 16

appendRand :: RandomGen g => g -> ByteString -> ByteString
appendRand g input = B.concat [before, input, after]
    where
        before = randByteString g  n1
        after  = randByteString g2 n2
        (n1, g2) = randomR (5, 10) g
        (n2, _ ) = randomR (5, 10) g2

cbcEncryptRand :: RandomGen g => g -> ByteString -> ByteString
cbcEncryptRand g = cbcEncrypt cipher iv . pkcs7 16
    where iv = maybe (error "invalid IV") id (makeIV $ randomAESKey g1)
          cipher = initAES128 key
          key = randomAESKey g2
          (g1, g2) = R.split g

ecbEncryptRand :: RandomGen g => g -> ByteString -> ByteString
ecbEncryptRand g = ecbEncrypt cipher . pkcs7 16
    where cipher = initAES128 key
          key = randomAESKey g

ecbDecryptRand :: RandomGen g => g -> ByteString -> ByteString
ecbDecryptRand g ct = ecbDecrypt cipher ct
    where cipher = initAES128 key
          key = randomAESKey g

cbcOrECB :: RandomGen g => g -> (ByteString -> ByteString)
cbcOrECB g
  | cipher == True = ecbEncryptRand gNext
  | otherwise      = cbcEncryptRand gNext
  where (cipher, gNext) = random g

encryptionOracle :: RandomGen g => g -> ByteString -> ByteString
encryptionOracle g = cbcOrECB g1 . appendRand g2
    where (g1, g2) = R.split g

-- tests whether we correctly detect what cipher the oracle would use by
-- rigging the cipher that is being used
challenge11 :: RandomGen g => g -> ByteString -> Bool
challenge11 g pt = used == guessed
    where (used, _) = random g
          (g1, g2) = R.split g
          guessed = detectECB 16 . rigged used g1 $ appendRand g2 pt
          rigged True  = ecbEncryptRand
          rigged False = cbcEncryptRand


-- Challenge 12

secret :: ByteString
secret = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"

-- byte-at-a-time ECB oracle
baatOracle :: RandomGen g => g -> ByteString -> ByteString
baatOracle g pt = ecbEncryptRand g (append pt $ decode64 secret)

-- after the first bs of padding sent through the cipher, the first block of
-- output will always be the same -> we know the block size
oracleBlockSize :: (ByteString -> ByteString) -> Int
oracleBlockSize oracle = P.head [bs | bs <- [2..], first bs == second bs]
    where first  bs = B.take bs (oracle $ B.replicate bs _A)
          second bs = B.take bs (oracle $ B.replicate (bs+1) _A)

-- does the oracle use an ECB cipher?
oracleECB :: (ByteString -> ByteString) -> Int -> Bool
oracleECB oracle bs = first == second
    where first = B.take bs whole
          second = B.take bs $ B.drop bs whole
          whole = oracle $ B.replicate (2 * bs) _A

headMay :: [a] -> Maybe a
headMay [] = Nothing
headMay (x:_) = Just x

-- guesses the next byte after based on some already guessed bytes and an
-- oracle
nextByte :: (ByteString -> ByteString) -> ByteString -> Maybe Word8
nextByte oracle guessed = headMay [byte | byte <- [minBound..], blockEqual pad (withByte byte)]
    where blockEqual a b = B.take (bs*blockN) (oracle a) == B.take (bs*blockN) (oracle b)
          withByte byte = snoc (append pad guessed) byte
          blockN = 1 + div (B.length guessed) bs -- block number
          padLen = bs * blockN - B.length guessed - 1 -- length of the padding
          pad = B.replicate padLen _A -- actual padding
          bs = oracleBlockSize oracle

baatDecryption :: (ByteString -> ByteString) -> ByteString
baatDecryption oracle = go ""
    where go guessed = maybe guessed (go . snoc guessed) (nextByte oracle guessed)


-- Challenge 13

-- email, uid, role, we don't use an Int for the id since we don't play with it
data Profile = Profile ByteString ByteString ByteString

instance Show Profile where
    show (Profile e u r) = show $ B.concat ["email: ", e, ", uid: ", u, ", role: ", r]

isAdmin :: Profile -> Bool
isAdmin (Profile _ _ r) = r == "admin"

-- the original functions use Strings instead of ByteStrings
bsImportParams :: ByteString -> [(ByteString, ByteString)]
bsImportParams input = maybe [] (P.map convert) list
    where convert (a, b) = (C.pack a, C.pack b)
          list = importParams (C.unpack input)

bsExportParams :: [(ByteString, ByteString)] -> ByteString
bsExportParams list = C.pack str
    where convert (a, b) = (C.unpack a, C.unpack b)
          str = exportParams $ P.map convert list

decodeProfile :: ByteString -> Maybe Profile
decodeProfile input = ap uidProfile (get "role")
    where
        uidProfile = ap emailProfile (get "uid")
        emailProfile = fmap Profile (get "email")
        get x = P.lookup x params
        params = bsImportParams input

encodeProfile :: Profile -> ByteString
encodeProfile (Profile e u r) = bsExportParams params
    where params = [("email", e), ("uid", u), ("role", r)]

profileFor :: ByteString -> Profile
profileFor e = Profile encoded "10" "user"
    where encoded = C.pack . encString True bad $ C.unpack e
          bad = not . flip P.elem ("&=" :: String)

encryptProfile :: RandomGen g => g -> Profile -> ByteString
encryptProfile g = ecbEncryptRand g . encodeProfile

decryptProfile :: RandomGen g => g -> ByteString -> Profile
decryptProfile g ct = fromMaybe err (decodeProfile $ ecbDecryptRand g ct)
    where err = error "invalid encrypted string"

-- takes an email and returns its encrypted profile
cutAndPasteOracle :: RandomGen g => g -> ByteString -> ByteString
cutAndPasteOracle g e = encryptProfile g $ profileFor e

-- GOAL:
-- email=h4ck@p0wn. com&uid=10&role= admin(whatever)
-- block 1          block 2          block 3
--
-- to get block 1 and 2, just call the oracle
-- to get block 3:
-- email=AAAAAAAAAA admin
ecbCutAndPaste :: (ByteString -> ByteString) -> ByteString
ecbCutAndPaste oracle = B.append b1And2 b3
    where b1And2 = B.take 32 $ oracle "h4ck@p0wn.com"
          b3 = B.take 16 $ B.drop 16 $ oracle "AAAAAAAAAAadmin"


-- Challenge 14

baatPrefixOracle :: RandomGen g => g -> ByteString -> ByteString
baatPrefixOracle g pt = baatOracle g (prefix <> pt)
    where prefix = randByteString g' len
          (len, g') = randomR (0,100) g

indexDuplicate :: Eq a => [a] -> Maybe Int
indexDuplicate l = go 0 l
    where go _ []  = Nothing
          go _ [_] = Nothing
          go i (x:x':xs)
            | x == x'   = Just i
            | otherwise = go (i+1) (x':xs)

ecbIndex :: Int -> ByteString -> Maybe Int
ecbIndex bs ct = fmap (* bs) (indexDuplicate blocks)
    where blocks = splitWithLength bs ct

-- therandomlengthp refixAAAAAAAAAAA AAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAA
--                       ----------- ^
--                         padLen    i
--
-- isPadLen returns True as soon as we get the same two blocks as with the huge
-- input
prefixLength :: Int -> (ByteString -> ByteString) -> Maybe Int
prefixLength bs oracle = liftM2 (-) i padLen
    where i = ecbIndex bs (oracle $ B.replicate 500 _A)
          padLen = L.find isPadLen [0..bs-1]
          isPadLen p = i == ecbIndex bs (oracle $ B.replicate (p + 2*bs) _A)

-- bruteforce the blocksize
prefixLengthAndBlockSize :: (ByteString -> ByteString) -> (Int, Int)
prefixLengthAndBlockSize oracle = go 2
    where go bs = case prefixLength bs oracle of
                    Just pl -> (pl, bs)
                    Nothing -> go (bs+1)

-- therandomlengthp refixAAAAAAAAAAA AAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAA
-- ----------------------
--       prefLen         -----------
--                         padLen
-- we pad the input to get a "plain" oracle after the prefix + padding and we
-- strip that from the result so that baatDecryption has something familiar to eat
baatPrefixDecryption :: (ByteString -> ByteString) -> ByteString
baatPrefixDecryption oracle = baatDecryption modOracle
    where modOracle pt = B.drop (prefLen + padLen) (oracle $ B.replicate padLen _A <> pt)
          padLen = bs - mod prefLen bs
          (prefLen, bs) = prefixLengthAndBlockSize oracle


-- Challenge 15

challenge15 :: ByteString -> Bool
challenge15 str
  | unPkcs7 str == Nothing = False
  | otherwise = True


-- Challenge 16

cbcBitflipOracle :: RandomGen g => g -> ByteString -> ByteString
cbcBitflipOracle g pt = cbcEncryptRand g (prefix <> encoded <> suffix)
    where prefix =  "comment1=cooking%20MCs;userdata="
          suffix =  ";comment2=%20like%20a%20pound%20of%20bacon"
          encoded = C.pack . encString True bad $ C.unpack pt
          bad = not . flip P.elem (";=" :: String)

cbcDecryptRand :: RandomGen g => g -> ByteString -> ByteString
cbcDecryptRand g ct = case unPkcs7 dec of
                        Just str -> str
                        Nothing  -> dec
    where dec = cbcDecrypt cipher iv ct
          iv = maybe (error "invalid IV") id (makeIV $ randomAESKey g1)
          cipher = initAES128 key
          key = randomAESKey g2
          (g1, g2) = R.split g

detectAdmin :: RandomGen g => g -> ByteString -> Bool
detectAdmin g = isInfixOf ";admin=true;" . cbcDecryptRand g

-- comment1=cooking %20MCs;userdata= AAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAA ;comment2...
-- --------------------------------- ---------------- ----------------
--                beg                       p1              p2
--                                                    -----------------------------
--                                                                 end
-- Let c1 and c2 be the ciphertext blocks for plaintexts p1 and p2.
-- We will modify c1 so that the decryption of c2 gives what we want.
-- The decryption of c2 involves XORing with c1. Since all operations are XORs,
-- we can "remove" p1 by XORing c1 with it, and "add" our payload the same way.
--
-- the decyption of c1 will now be incorrect but the decryption of c2 will be our payload
cbcBitflipAttack :: (ByteString -> ByteString) -> ByteString
cbcBitflipAttack oracle = beg <> mangled <> end
    where enc = oracle $ B.replicate 32 _A
          beg = B.take 32 enc
          mangled = fixedXOR ";admin=true;lmao" nullBlock
          nullBlock = fixedXOR (B.take 16 $ B.drop 32 enc) (B.replicate 16 _A)
          end = B.drop 48 enc
